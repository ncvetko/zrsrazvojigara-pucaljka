#ifndef __Y_AUDIOMGR_H
#define __Y_AUDIOMGR_H

#include <string>
#include <stdint.h>

#ifdef HAVE_SDL_MIXER


#if !defined(__APPLE__)
#include <SDL/SDL_mixer.h>
#else
#include <SDL_mixer/SDL_mixer.h>
#endif
#include <map>

struct AM_Channel {
    AM_Channel () : chunkid(-1), nextchunkid(-1) {

    }
    AM_Channel (int _chunkid, int _nextchunkid=-1) : chunkid(_chunkid), nextchunkid(_nextchunkid) {

    }

    int chunkid;
    int nextchunkid;
};

class AudioMgr {
	public:
		~AudioMgr();

		static AudioMgr* getInstance() { static AudioMgr* am=0; if (!am) am = new AudioMgr; return am; }
		void initAudio();
		void startMusic(std::string fn);
		void haltMusic();

		uint32_t loadSound(std::string fn);
		uint32_t playSound(uint32_t id, int repeat=0);
		void unloadSound(uint32_t id);
		void haltSound(uint32_t channel);
		void unloadSounds();

        static void channelFinished(int channel);

        void attachNextSound(int channel, int nextsound);

        void toggleSound() { enableSound(!m_allowSound); }
        void enableSound(bool enabled);
        bool isEnabledSound() { return m_allowSound; }
	private:
		AudioMgr();

		static void musicDone();

		int m_audioRate;
		uint16_t m_audioFormat;
		int m_audioChannels;
		int m_audioBuffers;

        std::string m_musicfn;
		Mix_Music *m_music;
		std::map<uint32_t, Mix_Chunk*> m_chunks;
		std::map<uint32_t, AM_Channel> m_channels;
		bool m_allowSound;

};

#else
class AudioMgr {
	public:
		~AudioMgr() {}

		static AudioMgr* getInstance() { static AudioMgr* am=0; if (!am) am = new AudioMgr; return am; }
		void initAudio() {}
		void startMusic(std::string fn) {}
		void haltMusic() {}

		uint32_t loadSound(std::string fn) {return 0;}
		uint32_t playSound(uint32_t id, int repeat=0) {return 0;}
		void unloadSound(uint32_t id) {}
		void haltSound(uint32_t channel) {}
		void unloadSounds() {}

		void attachNextSound(int channel, int nextsound) {}
        void toggleSound() {  }
        void enableSound(bool enabled) {}
        bool isEnabledSound() { return false; }

	private:
		AudioMgr() {}

};


#endif

#endif
