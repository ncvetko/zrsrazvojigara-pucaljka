#include "gm_mainmenu.h"
#ifndef __APPLE__
#include <GL/gl.h>
#include <GL/glu.h>
#else
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#endif
#include "button.h"
#include "layer.h"
#include "audiomgr.h"

extern bool gRunning;
extern SDL_Surface * gScreen;

GM_MainMenu::GM_MainMenu()
{
    Layer *layer(new Layer("background.png", gScreen->w / 2,gScreen->h / 2));
    this->layer.push_back(layer);

    Button *newgameButton(new NewGameButton("playbutton.png", 130, 100));
    this->buttons.push_back(newgameButton);

    Button *optionsButton(new OptionsButton("optionsbutton.png", 130, 160));
    this->buttons.push_back(optionsButton);

    Button *creditsButton(new CreditsButton("creditsbutton.png", 130, 220));
    this->buttons.push_back(creditsButton);

    Button *exitButton(new ExitButton("exitbutton.png", 130, 335));
    this->buttons.push_back(exitButton);

    AudioMgr::getInstance()->startMusic("Mad_Mav_-_Torture.mp3");
}
GM_MainMenu::~GM_MainMenu()
{

   for(std::vector<Layer*>::iterator itr = this->layer.begin();
        itr != this->layer.end();
        itr++)
        {
            Layer *bg = *itr;
            delete bg;
        }



    for(std::vector<Button*>::iterator it = this->buttons.begin();
        it != this->buttons.end();
        it++)
    {
        // it se ponasa kao da je: Button** it
        Button * btn = *it;
        delete btn;

        // alternativa: delete *it;
    }
}

void
GM_MainMenu::keyDown(const SDL_Event& event)
{
    switch(event.key.keysym.sym)
    {
        case SDLK_ESCAPE:
        gRunning = false;
        break;

        default:
        GameMode::keyDown(event);
        break;
    }


}

void
GM_MainMenu::paintSelf()
{
    glViewport(0, 0, gScreen->w, gScreen->h);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, 1024, 768, 0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    for(std::vector<Layer*>::iterator itr = this->layer.begin();
        itr != this->layer.end();
        itr++)
    {
        (*itr)->paintSelf();
    }

    for(std::vector<Button*>::iterator it = this->buttons.begin();
        it != this->buttons.end();
        it++)
    {
        (*it)->paintSelf();
    }

    SDL_GL_SwapBuffers();
}

void
GM_MainMenu::mouseDown(const SDL_Event& event)
{
    if(event.button.x < 30 && event.button.y < 30)
    {
        gRunning = false;
    }

    for(std::vector<Button*>::iterator it = this->buttons.begin();
        it != this->buttons.end();
        it++)
    {
        (*it)->mouseDown(event);
    }

}


void
GM_MainMenu::mouseUp(const SDL_Event& event)
{
    for(std::vector<Button*>::iterator it = this->buttons.begin();
        it != this->buttons.end();
        it++)
    {
        if((*it)->mouseUp(event))
        {
            return;
        }
    }
}

void
GM_MainMenu::mouseMove(const SDL_Event& event)
{
    for(std::vector<Button*>::iterator it = this->buttons.begin();
        it != this->buttons.end();
        it++)
    {
        (*it)->mouseMove(event);
    }
}
