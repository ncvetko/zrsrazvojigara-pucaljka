#include "layer.h"
#include "texture.h"
#include "texture_manager.h"



Layer::Layer(const std::string& textureFile,
                       const int argX,
                       const int argY) : texture(TextureManager::getInstance()->getTexture(textureFile)),
                                         x(argX), y(argY)
{
    this->texture->retain();
}
Layer::~Layer()
{
    this->texture->release();
}
void
Layer::paintSelf()
{
    this->texture->bind();
    glEnable(GL_TEXTURE_2D);

    glPushMatrix();
    glTranslatef(this->x, this->y, 0);
    glScalef(1024, 768, 0);
    glBegin(GL_TRIANGLES);

    glTexCoord2f(0, 0);
    glVertex2f(-0.5, -0.5);
    glTexCoord2f(0, 1);
    glVertex2f(-0.5, 0.5);
    glTexCoord2f(1, 1);
    glVertex2f(0.5, 0.5);

    glTexCoord2f(1, 1);
    glVertex2f(0.5, 0.5);
    glTexCoord2f(1, 0);
    glVertex2f(0.5, -0.5);
    glTexCoord2f(0, 0);
    glVertex2f(-0.5, -0.5);

    glEnd();

    glPopMatrix();

}

void
Layer::updateSelf(double timeDelta)
{
    // GameObject::updateSelf(timeDelta);
}
