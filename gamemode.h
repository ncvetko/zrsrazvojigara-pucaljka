#ifndef PUCALJKA_GAMEMODE_H
#define PUCALJKA_GAMEMODE_H

#include <SDL/SDL.h>

class GameMode
{
    public:
    GameMode()
    {
    }
    virtual ~GameMode()
    {
    }

    void switchGameMode(GameMode * gamemode);
    
    ////////////////////////////////////////
    
    virtual void keyDown(const SDL_Event& event)
    {
    }
    virtual void keyUp(const SDL_Event& event)
    {
    }
    virtual void mouseDown(const SDL_Event &event)
    {
    }
    virtual void mouseUp(const SDL_Event &event)
    {
    }
    virtual void mouseMove(const SDL_Event &event)
    {
    }

    virtual void paintSelf(void)
    {
    }

    virtual void updateSelf(double timeDelta)
    {
    }

    private:
};

extern GameMode * gGame;

#endif
