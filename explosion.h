#ifndef EXPLOSION_H
#define EXPLOSION_H

#include <SDL/SDL_events.h>
#include <string>
#include "texture.h"
#include "gameobject.h"

class Explosion : public GameObject
{
    public:
    Explosion(const std::string& textureFile,
               const double x,
               const double y);
    ~Explosion();
    void calcTexCoords(int spriteOffX, int spriteOffY,
                       float wImg, float hImg,
                       int wSprite, int hSprite,
                       int animation, int frame,
                       float *txTop, float *txBottom, float *txLeft, float *txRight);
   virtual void paintSelf();
   virtual void updateSelf(const double timeDelta);

    private:
    Texture * texture;
    double x, y;
    double t;
};


#endif
