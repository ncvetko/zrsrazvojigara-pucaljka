#include "playership.h"
#include "texture.h"
#include <SDL/SDL_events.h>
#include "bullet.h"
#include "gm_level.h"
#include "gameobject.h"
#include "gamemode.h"
#include "audiomgr.h"
#include "texture_manager.h"

static const double playerShipAngleV = 180;
static const double playerShipYV = 380;
extern int playerShipLives = 3;

extern SDL_Joystick * gJoystick;

PlayerShip::PlayerShip(const std::string& textureFile,
                       const double argX,
                       const double argY) : texture(TextureManager::getInstance()->getTexture(textureFile)),
                        x(argX), y(argY), angle(0), angleV(0), yv(0), timeUntilNextFiring(getFiringRate()), died(0)
{
    this->texture->retain();
    shootSound = AudioMgr::getInstance()->loadSound("gun.wav");
}
PlayerShip::~PlayerShip()
{
    this->texture->release();
    AudioMgr::getInstance()->unloadSound(shootSound);
}
void
PlayerShip::paintSelf()
{
    this->texture->bind();
    glEnable(GL_TEXTURE_2D);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(this->x, this->y, 0);
    glScalef(64, 64, 0);
    glRotatef(90, 0, 0, 1);
    glRotatef(angle, 0, 0, 1);
    glBegin(GL_TRIANGLES);


    glTexCoord2f(0, 0);
    glVertex2f(-0.5, -0.5);
    glTexCoord2f(0, 1);
    glVertex2f(-0.5, 0.5);
    glTexCoord2f(1, 1);
    glVertex2f(0.5, 0.5);

    glTexCoord2f(1, 1);
    glVertex2f(0.5, 0.5);
    glTexCoord2f(1, 0);
    glVertex2f(0.5, -0.5);
    glTexCoord2f(0, 0);
    glVertex2f(-0.5, -0.5);

    glEnd();
    glPopMatrix();

}

void
PlayerShip::updateSelf(double timeDelta)
{
    angle += angleV * timeDelta;
    y += yv * timeDelta;
    timeUntilNextFiring -= timeDelta;

    static bool prevJoyState = 0;
    bool currentJoyState = (gJoystick && SDL_JoystickGetButton(gJoystick, 0));

    if(SDL_GetKeyState(NULL)[SDLK_SPACE] ||
       currentJoyState)
    {
        if(currentJoyState && !prevJoyState)
            timeUntilNextFiring = 0;
        fire();
    }

    if(gJoystick)
    {
        Sint16 axisYInt = SDL_JoystickGetAxis(gJoystick, 1);
        double axisY = (axisYInt / 32767.);

        y += axisY * playerShipYV * timeDelta;
    }

    if(y < -240) y = -240;
    if(y > 240) y = 240;
    prevJoyState = currentJoyState;


    GM_Level * ownerLevel = this->getOwnerLevel();
    std::vector<GameObject*>& gameObjects(ownerLevel->getGameObjects());
    for(std::vector<GameObject*>::iterator it = gameObjects.begin(); it != gameObjects.end(); it++)
    {
        GameObject * currentObject = *it;

        if(this == currentObject)
            continue;

        EnemyShip * currentEnemyShip = dynamic_cast<EnemyShip*>(currentObject);
        if(currentEnemyShip) // if(currentEnemyShip != NULL)
        {
            PUCRect r(currentEnemyShip->getRect());
            if(this->x >= r.x && this->x <= r.x + r.w &&
               this->y >= r.y && this->y <= r.y + r.h)
            {
                currentEnemyShip->die();
                this->takenDamage();
                break;
            }
        }
    }
}

void
PlayerShip::keyDown(const SDL_Event &event)
{

    switch(event.key.keysym.sym)
    {
        case SDLK_RIGHT:
        angleV = playerShipAngleV;
        break;

        case SDLK_LEFT:
        angleV = -playerShipAngleV;
        break;

        case SDLK_UP:
        yv = -playerShipYV;
        break;

        case SDLK_DOWN:
        yv =  playerShipYV;
        break;

        case SDLK_SPACE:
        timeUntilNextFiring = 0;
        break;

        default:
        break;
    }
}


void
PlayerShip::keyUp(const SDL_Event &event)
{
    switch(event.key.keysym.sym)
    {
        case SDLK_RIGHT:
            angleV = 0;
            break;

        case SDLK_LEFT:
            angleV = 0;
            break;

        case SDLK_UP:
            yv = 0;
            break;
        case SDLK_DOWN:
            yv = 0;
			break;
        default:
            break;
    }

}

void
PlayerShip::mouseDown(const SDL_Event &event)
{

}

void
PlayerShip::mouseUp(const SDL_Event &event)
{

}

void
PlayerShip::mouseMove(const SDL_Event &event)
{

}

void
PlayerShip::fire(void)
{
    if(this->timeUntilNextFiring > 0)
        return;

    Bullet * b = new Bullet(this->x, this->y);
    GM_Level * currentLevel = (GM_Level*)gGame;

    AudioMgr::getInstance()->playSound(shootSound);
    currentLevel->addGameObject(b);

    this->timeUntilNextFiring = this->getFiringRate();
}
void
PlayerShip::takenDamage()
{
    playerShipLives -= 1;
    if(playerShipLives <= 0)
    {
        died = true;
        playerShipLives = 3;
    }

}

