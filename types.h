#ifndef PUCALJKA_TYPES_H
#define PUCALJKA_TYPES_H

typedef struct _PUCRect
{
    double x, y;
    double w, h;
} PUCRect;

#endif