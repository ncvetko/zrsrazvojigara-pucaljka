#ifndef PUCALJKA_TEXTURE_MANAGER_H
#define PUCALJKA_TEXTURE_MANAGER_H

#include <map>
#include <string>
class Texture;


class TextureManager
{
    public:
    TextureManager();
    ~TextureManager();

    static TextureManager* getInstance();
    Texture* getTexture(std::string filename);
    static void destroyInstance();
    bool isLoaded();
    void unloadTexture(const std::string& filename);

    private:
    std::map<std::string, Texture*> textures;
    static TextureManager* instance;
    bool bLoaded;



};


#endif
