#include <math.h>
#include "yellowenemyship.h"

YellowEnemyShip::YellowEnemyShip(const double x,
                                 const double y) : EnemyShip("enemyship03.png", x, y), defaultY(y)
{
}

YellowEnemyShip::~YellowEnemyShip()
{
}

void
YellowEnemyShip::updateSelf(const double timeDelta)
{
    EnemyShip::updateSelf(timeDelta);

    this->x -= 200 * timeDelta;

    double defaultYPositive = fabs(this->defaultY);
    if(this->defaultY > 0)
    {
        this->y = -((log(x + 320) / 6.4) * defaultYPositive*2 - defaultYPositive);
    }
    else
    {
        this->y = (log(x + 320) / 6.4) * defaultYPositive*2 - defaultYPositive;
    }
/*
    this->angle = atan2((0.3125 * 2 * defaultYPositive) / (this->x * 320.), this->x);
*/
}

