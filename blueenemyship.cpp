#include <math.h>
#include "blueenemyship.h"

BlueEnemyShip::BlueEnemyShip(const double x,
                                 const double y) : EnemyShip("enemyship02.png", x, y), baseY(y)
{
}

BlueEnemyShip::~BlueEnemyShip()
{
}

void
BlueEnemyShip::updateSelf(const double timeDelta)
{
    this->x-=200*timeDelta;
    this->y=(sin(x*M_PI*2 / 200)*75) + this->baseY;
}
