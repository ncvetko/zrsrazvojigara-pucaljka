#include "gamemode.h"

GameMode * gGame = NULL;

void GameMode::switchGameMode(GameMode *gameMode)
{
    GameMode * oldGameMode = gGame;
    gGame = gameMode;
    delete oldGameMode;
}