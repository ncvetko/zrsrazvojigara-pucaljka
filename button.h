#ifndef PUCALJKA_BUTTON_H
#define PUCALJKA_BUTTON_H

#include <SDL/SDL_events.h>
#include <string>
#include "texture.h"

class Button
{
    public:
    Button(const std::string& textureFile,
           const double x,
           const double y);
    virtual ~Button();

    enum State
    {
        STATE_UP = 0,
        STATE_DOWN = 1,
        STATE_OVER = 2,
        STATE_DISABLED = 3
    };

    bool mouseInside(const float mouseX,
                     const float mouseY) const
    {
        const int w = texture->getW();
        const int h = texture->getH();
        const float mX = mouseX + w / 2;
        const float mY = mouseY + h / 2;

        return (mX > x && mX < x + w &&
                mY > y && mY < y + h);
    }

    void paintSelf();
    void updateSelf(const double timeDelta);

    bool mouseUp(const SDL_Event &event);
    void mouseDown(const SDL_Event &event);
    void mouseMove(const SDL_Event &event);

    virtual bool handleClick();

    private:
    Texture * texture;
    float x, y;
    State state;
};

class ExitButton : public Button
{
    public:
    ExitButton(const std::string& textureFile,
               const double x,
               const double y) :
               Button(textureFile, x, y)
    {
    }
    bool handleClick();
};
class NewGameButton : public Button
{
    public:
     NewGameButton(const std::string& textureFile,
                const double x,
                const double y) :
                Button(textureFile, x, y)
    {
    }

    bool handleClick();

};
class OptionsButton : public Button
{
    public:
    OptionsButton(const std::string& textureFile,
                const double x,
                const double y) :
                Button(textureFile, x, y)
    {
    }
    bool handleClick();

};
class CreditsButton : public Button
{
    public:
    CreditsButton(const std::string& textureFile,
                const double x,
                const double y) :
                Button(textureFile, x, y)
    {
    }
    bool handleClick();
};

class BackToMainMenuButton : public Button
{
    public:
    BackToMainMenuButton(const std::string& textureFile,
                         const double x,
                         const double y) :
                         Button(textureFile, x, y)
    {
    }
    bool handleClick();
};

#endif
