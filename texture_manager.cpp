#include "texture_manager.h"
#include "texture.h"
#include <stdio.h>

TextureManager* TextureManager::instance;

TextureManager::TextureManager()
{
    bLoaded = false;
}

TextureManager::~TextureManager()
{

}

TextureManager* TextureManager::getInstance()
{
    if(!instance)
        instance = new TextureManager();
    return instance;
}

void
TextureManager::destroyInstance()
{
    delete instance;
    instance = NULL;
}

bool
TextureManager::isLoaded()
{
    return this->bLoaded;
}

Texture* TextureManager::getTexture(std::string filename)
{
    if(textures.count(filename) == 0)
    {
        Texture * texture = new Texture(filename);
        std::map<std::string, Texture*>::iterator it = textures.end();
        textures.insert(it, std::pair<std::string, Texture*>(filename, texture));
        return texture;

    }
    else
    {
        return textures.find(filename)->second;
    }

}
void TextureManager::unloadTexture(const std::string &filename)
{
    std::map<std::string, Texture*>::iterator it = textures.find(filename);
    if(it == textures.end())
        return; // already unloaded

    printf("Unloading %s\n", filename.c_str());
    delete it->second;
    textures.erase(it);
}
