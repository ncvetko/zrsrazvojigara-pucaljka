#ifndef __APPLE__
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/glu.h>
#else
#include <SDL/SDL.h>
#include <SDL_image/SDL_image.h>
#include <OpenGL/glu.h>
#endif
#include "texture.h"
#include "texture_manager.h"

Texture::Texture(const std::string& textureFile) : refCount(0), textureFile(textureFile)
{
    this->_loadTexture();
}

Texture::~Texture()
{
    glDeleteTextures(1, &this->textureId);
}

void Texture::_loadTexture()
{
    GLuint tid;
    const char * file = this->textureFile.c_str();

    printf("Loading %s\n", file);
    SDL_Surface *img = IMG_Load(file);
    if(!img)
    {
        printf("Loading file %s failed\n", file);
        return;
    }
    SDL_LockSurface(img);

    glGenTextures(1, &tid);
    glBindTexture(GL_TEXTURE_2D, tid);

    this->w = img->w;
    this->h = img->h;

    gluBuild2DMipmaps(GL_TEXTURE_2D,
                      img->format->BytesPerPixel == 4 ? GL_RGBA : GL_RGB,
                      img->w,
                      img->h,
#if __APPLE__
                      img->format->BytesPerPixel == 4 ? GL_BGRA : GL_BGR,
#else
                      img->format->BytesPerPixel == 4 ? GL_RGBA : GL_RGB,
#endif
                      GL_UNSIGNED_BYTE,
                      img->pixels);

    SDL_UnlockSurface(img);
    SDL_FreeSurface(img);

    this->textureId = tid;
}

void Texture::bind()
{
    glBindTexture(GL_TEXTURE_2D, this->textureId);
}

void Texture::release()
{
    this->refCount--;
    if(this->refCount >= 1)
        return;
    
    TextureManager::getInstance()->unloadTexture(this->getFilename());
}
