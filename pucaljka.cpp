#include <stdio.h>
#include <SDL/SDL.h>
#include "gamemode.h"
#include "gameobject.h"
#include "gm_mainmenu.h"
#include "gm_level.h"
#include "audiomgr.h"
#include <time.h>

SDL_Surface * gScreen = NULL;
bool gRunning = true;
SDL_Joystick * gJoystick = NULL;

/*****************************/

int
main(int argc,
     char ** argv)
{
    srand(time(NULL));

    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_JOYSTICK);
    SDL_WM_SetCaption("Pucaljka", "Pucaljka");
    gScreen = SDL_SetVideoMode(1024, 768, 0, SDL_OPENGL);

    if(SDL_NumJoysticks() > 0)
    {
        printf("Opening joystick %s\n", SDL_JoystickName(0));
        gJoystick = SDL_JoystickOpen(0);
    }

    AudioMgr::getInstance()->initAudio();

    gGame = new GM_MainMenu();
    uint32_t OldTime = SDL_GetTicks();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    while(gRunning)
    {
        SDL_Event event;
        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_QUIT:
                gRunning = false;
                break;

                case SDL_KEYDOWN:
                gGame->keyDown(event);
                break;

                case SDL_KEYUP:
                gGame->keyUp(event);
                break;

                case SDL_MOUSEBUTTONDOWN:
                gGame->mouseDown(event);
                break;

                case SDL_MOUSEBUTTONUP:
                gGame->mouseUp(event);
                break;

                case SDL_MOUSEMOTION:
                gGame->mouseMove(event);
                break;
            }
        }


        uint32_t NewTime = SDL_GetTicks();
        uint32_t differenceInMiliseconds = NewTime - OldTime;
        OldTime = NewTime;
        float dt = differenceInMiliseconds / 1000.0;

        if(dt > 0.2)
            dt = 0.2;

        gGame->paintSelf();
        gGame->updateSelf(dt);



    }

    delete gGame;

    SDL_Quit();

    return 0;
}

