#ifndef PUCALJKA_TEXTURE_H
#define PUCALJKA_TEXTURE_H

#include <string>
#ifndef __APPLE__
#include <GL/gl.h>
#else
#include <OpenGL/gl.h>
#endif

class Texture
{
    public:
    Texture(const std::string& textureFile);
    ~Texture();
    void bind();

    int getW() const { return this->w; }
    int getH() const { return this->h; }
    const std::string& getFilename() const { return this->textureFile; }

    void retain() { this->refCount++; }
    void release();
    
    private:
    std::string textureFile;
    GLuint textureId;
    int w, h;
    int refCount;

    void _loadTexture();
};

#endif
