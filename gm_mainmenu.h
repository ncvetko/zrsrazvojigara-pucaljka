#ifndef PUCALJKA_GM_MAINMENU_H
#define PUCALJKA_GM_MAINMENU_H

#include <SDL/SDL.h>
#include <vector>
#include "gamemode.h"


class Button;
class Layer;

class GM_MainMenu : public GameMode
{
    public:
    GM_MainMenu();
    ~GM_MainMenu();

    void keyDown(const SDL_Event& event);
    void paintSelf(void);
    void mouseUp(const SDL_Event &event);
    void mouseDown(const SDL_Event &event);
    void mouseMove(const SDL_Event &event);

    private:
    std::vector<Button*> buttons;
    std::vector<Layer*> layer;
};

#endif
