#include "enemyship.h"
#include "texture.h"
#include "texture_manager.h"
#include <math.h>

bool gDrawCollisionRects;

EnemyShip::EnemyShip(const std::string& textureFile,
               const double argX,
               const double argY) : texture(TextureManager::getInstance()->getTexture(textureFile)),
                                    x(argX), y(argY), angle(0)
{
    this->texture->retain();
}
EnemyShip::~EnemyShip()
{
    this->texture->release();
}

void
EnemyShip::paintSelf()
{
    if(this->isDead())
        glColor4f(1, 0, 0, 1);

    this->texture->bind();
    glEnable(GL_TEXTURE_2D);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(this->x, this->y, 0);
    glScalef(this->getShipW(), this->getShipH(), 0);
    glRotatef(-90, 0, 0, 1);
    glRotatef(this->angle * 180 / M_PI, 0, 0, 1);
    glBegin(GL_TRIANGLES);


    glTexCoord2f(0, 0);
    glVertex2f(-0.5, -0.5);
    glTexCoord2f(0, 1);
    glVertex2f(-0.5, 0.5);
    glTexCoord2f(1, 1);
    glVertex2f(0.5, 0.5);

    glTexCoord2f(1, 1);
    glVertex2f(0.5, 0.5);
    glTexCoord2f(1, 0);
    glVertex2f(0.5, -0.5);
    glTexCoord2f(0, 0);
    glVertex2f(-0.5, -0.5);

    glEnd();
    glPopMatrix();


    glColor4f(1, 1, 1, 1);

    if(gDrawCollisionRects)
    {
        glDisable(GL_TEXTURE_2D);
        glBegin(GL_LINE_LOOP);
        glVertex2f(getRect().x, getRect().y);
        glVertex2f(getRect().x + getRect().w, getRect().y);
        glVertex2f(getRect().x + getRect().w, getRect().y + getRect().h);
        glVertex2f(getRect().x, getRect().y + getRect().h);
        glEnd();
    }
}

void
EnemyShip::updateSelf(const double timeDelta)
{
}
