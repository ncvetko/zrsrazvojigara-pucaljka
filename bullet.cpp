#include "bullet.h"
#include "texture.h"
#include "playership.h"
#include "gm_level.h"
#include "texture_manager.h"
#include "explosion.h"

Bullet::Bullet( double argX, double argY) : texture(TextureManager::getInstance()->getTexture("bullet.png")),
                                            x(argX), y(argY)
{
    this->texture->retain();
}
Bullet::~Bullet()
{
    this->texture->release();
}
void
Bullet::updateSelf(const double timeDelta)
{
	this->x+=320 * timeDelta;

    GM_Level * ownerLevel = this->getOwnerLevel();
    std::vector<GameObject*>& gameObjects(ownerLevel->getGameObjects());
    for(std::vector<GameObject*>::iterator it = gameObjects.begin(); it != gameObjects.end(); it++)
    {
        GameObject * currentObject = *it;

        if(this == currentObject)
            continue;

        EnemyShip * currentEnemyShip = dynamic_cast<EnemyShip*>(currentObject);
        if(currentEnemyShip) // if(currentEnemyShip != NULL)
        {
            PUCRect r(currentEnemyShip->getRect());
            if(this->x >= r.x && this->x <= r.x + r.w &&
               this->y >= r.y && this->y <= r.y + r.h)
            {
                Explosion *explosion(new Explosion("text.png", currentEnemyShip->x, currentEnemyShip->y));
                ownerLevel->addGameObject(explosion, true);

                currentEnemyShip->die();
                this->die();
                break;
            }
        }
    }

}


void
Bullet::paintSelf()
{

this->texture->bind();
    glEnable(GL_TEXTURE_2D);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(this->x, this->y, 0);
	glBegin(GL_TRIANGLES);


    glTexCoord2f(0, 0);
    glVertex2f(-0.5, -0.5);
    glTexCoord2f(0, 1);
    glVertex2f(-0.5, 0.5);
    glTexCoord2f(1, 1);
    glVertex2f(0.5, 0.5);

    glTexCoord2f(1, 1);
    glVertex2f(0.5, 0.5);
    glTexCoord2f(1, 0);
    glVertex2f(0.5, -0.5);
    glTexCoord2f(0, 0);
    glVertex2f(-0.5, -0.5);

    glEnd();
    glPopMatrix();

}
