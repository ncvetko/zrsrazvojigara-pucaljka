#include<math.h>
#include "levelbackground.h"
#include "texture.h"
#include "gm_level.h"


LevelBackground::LevelBackground(const int argX,
                                 const int argY) : Layer ("lvlbackground1.png", 0,0), xOffset(0), yOffset(0)
{
}
LevelBackground::~LevelBackground()
{
}

void
LevelBackground::paintSelf()
{
    glMatrixMode(GL_TEXTURE);
    glPushMatrix();
    glTranslatef(xOffset, yOffset, 0);

    glMatrixMode(GL_MODELVIEW);

    Layer::paintSelf();

    glMatrixMode(GL_TEXTURE);
    glPopMatrix();

    glMatrixMode(GL_MODELVIEW);

}

void
LevelBackground::updateSelf(const double timeDelta)
{
    Layer::updateSelf(timeDelta);
    xOffset += timeDelta / 25;
}
