#ifndef PUCALJKA_GM_LEVEL_H
#define PUCALJKA_GM_LEVEL_H

#include <SDL/SDL.h>
#include <vector>
#include "gamemode.h"
#include "gameobject.h"
#include "enemyship.h"
#include "bullet.h"
#include "levelbackground.h"

class PlayerShip;
class Layer;
class RedEnemyShip;
class BlueEnemyShip;
class Asteroid;
class LevelBackground;
class Texture;

class GM_Level : public GameMode
{
    public:
    GM_Level();
    ~GM_Level();

    void keyUp(const SDL_Event& event);
    void keyDown(const SDL_Event& event);
    void paintSelf(void);
    void mouseUp(const SDL_Event &event);
    void mouseDown(const SDL_Event &event);
    void mouseMove(const SDL_Event &event);
    void updateSelf(double timeDelta);

    void addGameObject(const GameObject *gameObject, bool toFront = false);

    void spawnAsteroid(double x, double y);
    void drawHealth(double x, double y);

    std::vector<GameObject*>& getGameObjects()
    {
        return this->gameObjects;
    }

    private:
    std::vector<GameObject*> gameObjects;
    std::vector<Layer*> layers;
    std::vector<Asteroid*> asteroids;
    PlayerShip * playership;
    LevelBackground * levelbackground;
    Texture * texture;
    double x, y;

    std::vector<GameObject*> gameObjectsToInsertToFront;
    std::vector<GameObject*> gameObjectsToInsertToBack;
    void flushInsertedAndDeadObjects();

};

#endif

