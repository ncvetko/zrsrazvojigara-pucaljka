#ifndef PUCALJKA_PLAYERSHIP_H
#define PUCALJKA_PLAYERSHIP_H

#include <SDL/SDL_events.h>
#include <string>
#include "texture.h"
#include "gameobject.h"

class PlayerShip : public GameObject
{
    public:
    PlayerShip(const std::string& textureFile,
               const double x,
               const double y);
    virtual ~PlayerShip();

    void keyDown(const SDL_Event& event);
    void keyUp(const SDL_Event& event);
    void mouseDown(const SDL_Event &event);
    void mouseUp(const SDL_Event &event);
    void mouseMove(const SDL_Event &event);
    void paintSelf(void);
    void updateSelf(double timeDelta);
    void takenDamage();

    void fire(void);

    const double getX () { return x;};
    const double getY () { return y;};

    const bool isDead () {return died; };
    const double getFiringRate() { return 0.2; }

    private:
    Texture * texture;
    double x, y;
    double angle;
    double angleV;
    double yv;
    double timeUntilNextFiring;
    int shootSound;
    bool died;
};
#endif

