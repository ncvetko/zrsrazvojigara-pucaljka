#ifndef WIN32
    #include <err.h>
#else
    #define warnx
#endif

#include "audiomgr.h"

#if 1
#ifdef HAVE_SDL_MIXER

#if !defined(__APPLE__)
#include <SDL/SDL_mixer.h>
#else
#include <SDL_mixer/SDL_mixer.h>
#endif



AudioMgr::AudioMgr() {

	m_music = NULL;
	initAudio();
	m_allowSound = true;
}

AudioMgr::~AudioMgr()
{
	haltMusic();
	Mix_CloseAudio();
}


void AudioMgr::initAudio()
{
	m_audioRate = 22050;
	m_audioFormat = AUDIO_S16;
	m_audioChannels = 2;
	m_audioBuffers = 4096;

	if(Mix_OpenAudio(m_audioRate, m_audioFormat, m_audioChannels, m_audioBuffers)) {
		warnx("unable to open audio!");
	}
	Mix_ChannelFinished(AudioMgr::channelFinished);

}

void AudioMgr::startMusic(std::string fn)
{
    std::string s = fn; //y_findfile(fn.c_str(), "r");
    if (m_musicfn == s) {
        return;
    }


	if (m_music) {
		haltMusic();
	}


    m_musicfn = s;
	m_music = Mix_LoadMUS(s.c_str());
	Mix_PlayMusic(m_music, 0);
	if (!m_allowSound)
        Mix_PauseMusic();
	Mix_HookMusicFinished(musicDone);
}

void AudioMgr::haltMusic() {
	Mix_HaltMusic();
	Mix_FreeMusic(m_music);
	m_musicfn = "";
	m_music = NULL;
}


void AudioMgr::musicDone()
{
	Mix_PlayMusic(getInstance()->m_music, 0);
}


uint32_t AudioMgr::loadSound(std::string fn)
{
	int id = rand();
	printf("Loading sound %s\n", fn.c_str());
    std::string s = fn; //y_findfile(fn.c_str(), "r");

	m_chunks[id] = Mix_LoadWAV(s.c_str());
	return id;
}

uint32_t AudioMgr::playSound(uint32_t id, int repeat)
{
    if (!m_allowSound) return -1;
	std::map<uint32_t, Mix_Chunk*>::iterator it = m_chunks.find(id);
	if (it == m_chunks.end()) return -1;
	int channel = Mix_PlayChannel(-1, it->second, repeat);
	m_channels[channel] = AM_Channel(id);
	return channel;
}

void AudioMgr::unloadSound(uint32_t id)
{
    if (id == -1) return;
	std::map<uint32_t, Mix_Chunk*>::iterator it = m_chunks.find(id);
	if (it != m_chunks.end()) {
        Mix_FreeChunk(it->second);
        m_chunks.erase(it);
	}

}

void AudioMgr::haltSound(uint32_t channel)
{
	Mix_HaltChannel(channel);
}


void AudioMgr::unloadSounds()
{
	for (std::map<uint32_t, Mix_Chunk*>::iterator it = m_chunks.begin(); it != m_chunks.end(); it++) {
		Mix_FreeChunk(it->second);
	}
	m_chunks.clear();
}

void AudioMgr::channelFinished(int channel)
{
    std::map<uint32_t, AM_Channel>::iterator chit = getInstance()->m_channels.find(channel);
    if (chit == getInstance()->m_channels.end())
        printf("AudioMgr::channelFinished(): channel %d not found playing\n", channel);
    else {
        if (chit->second.nextchunkid != -1)
            getInstance()->m_channels[channel] = AudioMgr::getInstance()->playSound(chit->second.nextchunkid);
        else
            getInstance()->m_channels.erase(chit);
    }
    printf("AudioMgr::channelFinished(): Removed.\n");
}

void AudioMgr::attachNextSound(int channel, int nextsound)
{
    std::map<uint32_t, AM_Channel>::iterator chit = m_channels.find(channel);
    if (chit == m_channels.end())
        printf("AudioMgr::attachNextSound(): channel %d not found playing\n", channel);
    else
        chit->second.nextchunkid = nextsound;

}
void AudioMgr::enableSound(bool enabled)
{
    m_allowSound = enabled;
    if (!m_allowSound) {
        Mix_PauseMusic();
    } else {
        Mix_ResumeMusic();
    }
}
#endif


#else

AudioMgr::AudioMgr()
{
}

AudioMgr::~AudioMgr()
{
}

void AudioMgr::initAudio()
{
}

void AudioMgr::startMusic(std::string fn)
{
}

void AudioMgr::haltMusic()
{
}

void AudioMgr::musicDone()
{
}

uint32_t AudioMgr::loadSound(std::string fn)
{
}

uint32_t AudioMgr::playSound(uint32_t id, int repeat)
{
}

void AudioMgr::unloadSound(uint32_t id)
{
}

void AudioMgr::haltSound(uint32_t channel)
{
}

void AudioMgr::unloadSounds()
{
}

void AudioMgr::channelFinished(int channel)
{
}

void AudioMgr::attachNextSound(int,int)
{
}

void AudioMgr::enableSound(bool enabled)
{
}

#endif
