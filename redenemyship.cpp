#include <math.h>
#include "redenemyship.h"

RedEnemyShip::RedEnemyShip(const double x,
                                 const double y) : EnemyShip("enemyship01.png", x, y), baseY(y)
{
}

RedEnemyShip::~RedEnemyShip()
{
}

void
RedEnemyShip::updateSelf(const double timeDelta)
{
    this->x-=200*timeDelta;
    this->y=fabs(x)-150;
    if(this->y > 0)
    {
        y=0;
    }
}

