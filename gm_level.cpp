#include "gm_level.h"
#ifndef __APPLE__
#include <GL/gl.h>
#include <GL/glu.h>
#else
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#endif
#include "gm_mainmenu.h"
#include "gameobject.h"
#include "layer.h"
#include "enemyship.h"
#include "playership.h"
#include "yellowenemyship.h"
#include "redenemyship.h"
#include "blueenemyship.h"
#include "bullet.h"
#include "asteroid.h"
#include "levelbackground.h"
#include "explosion.h"
#include "texture_manager.h"


extern bool gRunning;
extern SDL_Surface * gScreen;
extern int playerShipLives;

void
GM_Level::addGameObject(const GameObject *gameObject, bool toFront)
{
    if(!toFront)
        this->gameObjectsToInsertToBack.push_back((GameObject*)gameObject);
    else
        this->gameObjectsToInsertToFront.push_back((GameObject*)gameObject);
}
void
GM_Level::spawnAsteroid(double x, double y)
{
    Asteroid *asteroid(new Asteroid(x, y, 60 + 200*(rand() % 10000) / 10000. ));
    this->asteroids.push_back(asteroid);
}
void
GM_Level::drawHealth(double x, double y)
{
    this->texture->bind();
    glEnable(GL_TEXTURE_2D);

    glPushMatrix();
    glTranslatef(x, y, 0);
    glScalef(16, 32, 0);
    glBegin(GL_TRIANGLES);

    glTexCoord2f(0, 0);
    glVertex2f(-0.5, -0.5);
    glTexCoord2f(0, 1);
    glVertex2f(-0.5, 0.5);
    glTexCoord2f(1, 1);
    glVertex2f(0.5, 0.5);

    glTexCoord2f(1, 1);
    glVertex2f(0.5, 0.5);
    glTexCoord2f(1, 0);
    glVertex2f(0.5, -0.5);
    glTexCoord2f(0, 0);
    glVertex2f(-0.5, -0.5);

    glEnd();

    glPopMatrix();

}

GM_Level::~GM_Level()
{
    delete playership;
}

GM_Level::GM_Level() : playership(new PlayerShip("playership.png", -250, 200)),
                       texture(TextureManager::getInstance()->getTexture("health.png")),
                                            x(x), y(y)
{
    /*
    Layer *layer(new Layer("background.png", 0,0));
    this->layers.push_back(layer);
     */

    LevelBackground *layer(new LevelBackground(0,0));
    this->layers.push_back(layer);

    EnemyShip *enemyShip(new EnemyShip("enemyship01.png", 600, 400));
    this->addGameObject(enemyShip);

    EnemyShip *blueenemyShip(new EnemyShip("enemyship02.png", 800, 400));
    this->addGameObject(blueenemyShip);

    RedEnemyShip *redEnemyShip(new RedEnemyShip(320, 0));
    this->addGameObject(redEnemyShip);

    BlueEnemyShip *blueEnemyShip(new BlueEnemyShip(320, 0));
    this->addGameObject(blueEnemyShip);

    for(int i = 0; i < 50; i++)
    {
        YellowEnemyShip *yellowEnemyShip(new YellowEnemyShip(320 + i * 60, 240));
        this->addGameObject(yellowEnemyShip);
    }

    for(int i = 0; i < 50; i++)
    {
        YellowEnemyShip *yellowEnemyShip(new YellowEnemyShip(320 + i * 60, -240));
        this->addGameObject(yellowEnemyShip);
    }


    for(int i = 0; i < 50; i++)
    {
        YellowEnemyShip *yellowEnemyShip(new YellowEnemyShip(320 + i * 60, 120));
        this->addGameObject(yellowEnemyShip);
    }

    for(int i = 0; i < 50; i++)
    {
        YellowEnemyShip *yellowEnemyShip(new YellowEnemyShip(320 + i * 60, -120));
        this->addGameObject(yellowEnemyShip);
    }

 /*   if(enemyShip->getShouldDie()== true)
    {
        Explosion *explosion(new Explosion("text.png", 50, 50));
        this->addGameObject(explosion);
    }*/
}

void
GM_Level::keyDown(const SDL_Event& event)
{
    switch(event.key.keysym.sym)
    {
        case SDLK_ESCAPE:
        gGame->switchGameMode(new GM_MainMenu());
        break;

        case SDLK_LEFT:
        playership->keyDown(event);
        break;

        case SDLK_RIGHT:
        playership->keyDown(event);
        break;

        case SDLK_UP:
        playership->keyDown(event);
        break;

        case SDLK_DOWN:
        playership->keyDown(event);
        break;

        case SDLK_SPACE:
        playership->keyDown(event);
        break;

        case SDLK_c:
        gDrawCollisionRects = !gDrawCollisionRects;
        break;

        default:
        GameMode::keyDown(event);
        break;
    }


}
void
GM_Level::keyUp(const SDL_Event &event)
{
    this->playership->keyUp(event);
}

void
GM_Level::paintSelf()
{
    glViewport(0, 0, gScreen->w, gScreen->h);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluOrtho2D(-320, 320, 240, -240);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    for(std::vector<Layer*>::iterator it = layers.begin(); it != layers.end(); it++)
    {
        (*it)->paintSelf();
    }
    for(std::vector<Asteroid*>::iterator it = asteroids.begin(); it != asteroids.end(); it++)
    {
        (*it)->paintSelf();
    }

    playership->paintSelf();
    for(std::vector<GameObject*>::iterator it = gameObjects.begin(); it != gameObjects.end(); it++)
    {
        (*it)->paintSelf();
    }
    for(int i = playerShipLives; i > 0; i--)
    {
        this->drawHealth((-320 + this->texture->getW() / 2 - 17) + i * 17, (-240 + this->texture->getH()/ 2));
    }





    SDL_GL_SwapBuffers();
}

void
GM_Level::mouseDown(const SDL_Event& event)
{
    this->playership->mouseDown(event);
}

void
GM_Level::mouseUp(const SDL_Event& event)
{
    this->playership->mouseUp(event);
}


void
GM_Level::mouseMove(const SDL_Event& event)
{
    this->playership->mouseMove(event);
}
void GM_Level::updateSelf(double timeDelta)
{
    if(rand() % 1000 < 1000 * timeDelta )
    {
        double randomNumberX = 320 + 40;
        double randomNumberY = 240 - ((rand() % 10000) / 10000.) * 480;
        spawnAsteroid(randomNumberX, randomNumberY);
    }
    this->playership->updateSelf(timeDelta);
    for(std::vector<Layer*>::iterator it = this->layers.begin(); it != this->layers.end(); it++)
    {
        (*it)->updateSelf(timeDelta);
    }
    for(std::vector<Asteroid*>::iterator it = this->asteroids.begin(); it != this->asteroids.end(); it++)
    {
        (*it)->updateSelf(timeDelta);
    }
    this->playership->updateSelf(timeDelta);
    for(std::vector<GameObject*>::iterator it = this->gameObjects.begin(); it != this->gameObjects.end(); it++)
    {
        (*it)->updateSelf(timeDelta);
    }
    this->flushInsertedAndDeadObjects();
    if(playership->isDead())
    {
        gGame->switchGameMode(new GM_MainMenu());
        printf("Game Over");
    }
}

void GM_Level::flushInsertedAndDeadObjects()
{
    for(std::vector<GameObject*>::iterator it = this->gameObjects.begin(); it != this->gameObjects.end();)
    {
        if((*it)->isDead())
        {
            delete *it;
            this->gameObjects.erase(it);
            it = this->gameObjects.begin();
        }
        else
        {
            it++;
        }
    }

    this->gameObjects.insert(this->gameObjects.begin(), this->gameObjectsToInsertToFront.rbegin(), this->gameObjectsToInsertToFront.rend());
    this->gameObjects.insert(this->gameObjects.end(), this->gameObjectsToInsertToBack.begin(), this->gameObjectsToInsertToBack.end());

    this->gameObjectsToInsertToFront.clear();
    this->gameObjectsToInsertToBack.clear();
}
