#include "asteroid.h"
#include "texture.h"
#include "texture_manager.h"

Asteroid::Asteroid(const double argX,
                   const double argY,
                   const double argVY) : texture(TextureManager::getInstance()->getTexture("asteroid.png")),
                                         x(argX), y(argY), vy(argVY)
{
    this->texture->retain();
}
Asteroid::~Asteroid()
{
    this->texture->release();
}
void
Asteroid::paintSelf()
{
    this->texture->bind();
    glEnable(GL_TEXTURE_2D);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(this->x, this->y, 0);
    glScalef(32, 32, 0);
    glBegin(GL_TRIANGLES);


    glTexCoord2f(0, 0);
    glVertex2f(-0.5, -0.5);
    glTexCoord2f(0, 1);
    glVertex2f(-0.5, 0.5);
    glTexCoord2f(1, 1);
    glVertex2f(0.5, 0.5);

    glTexCoord2f(1, 1);
    glVertex2f(0.5, 0.5);
    glTexCoord2f(1, 0);
    glVertex2f(0.5, -0.5);
    glTexCoord2f(0, 0);
    glVertex2f(-0.5, -0.5);

    glEnd();
    glPopMatrix();

}
void
Asteroid::updateSelf(const double timeDelta)
{
    this->x-=vy*timeDelta;
}
