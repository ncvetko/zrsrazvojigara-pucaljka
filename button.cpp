#include "button.h"
#include "texture.h"
#include "gm_level.h"
#include "gm_credits.h"
#include "gm_mainmenu.h"
#include "gamemode.h"
#include "texture_manager.h"

extern bool gRunning;

Button::Button(const std::string& textureFile,
               const double argX,
               const double argY) : texture(TextureManager::getInstance()->getTexture(textureFile)),
                                    x(argX), y(argY)
{
    this->texture->retain();
}
Button::~Button()
{
    this->texture->release();
}

void Button::paintSelf()
{
    this->texture->bind();
    glEnable(GL_TEXTURE_2D);

    switch(state)
    {
        case STATE_UP:
        glColor3f(0.7, 0.7, 0.7);
        break;

        case STATE_OVER:
        glColor3f(1.0, 1.0, 1.0);
        break;

        case STATE_DOWN:
        glColor3f(0.3, 0.3, 0.3);
        break;

        case STATE_DISABLED:
        glColor3f(0.1, 0.1, 0.1);
        break;
    }

    glPushMatrix();
    glTranslatef(this->x, this->y, 0);
    glScalef(this->texture->getW(), this->texture->getH(), 0);
    glBegin(GL_TRIANGLES);

    glTexCoord2f(0, 0);
    glVertex2f(-0.5, -0.5);
    glTexCoord2f(0, 1);
    glVertex2f(-0.5, 0.5);
    glTexCoord2f(1, 1);
    glVertex2f(0.5, 0.5);

    glTexCoord2f(1, 1);
    glVertex2f(0.5, 0.5);
    glTexCoord2f(1, 0);
    glVertex2f(0.5, -0.5);
    glTexCoord2f(0, 0);
    glVertex2f(-0.5, -0.5);

    glEnd();

    glPopMatrix();

    glColor3f(1.0, 1.0, 1.0);
}

void Button::updateSelf(const double timeDelta)
{
}


bool Button::mouseUp(const SDL_Event &event)
{

    if(state == STATE_DOWN)
    {
        if(mouseInside(event.button.x, event.button.y))
        {
            if(this->handleClick())
            {
                return true;
            }
            state = STATE_OVER;
        }
        else
        {
            state = STATE_UP;
        }
    }
    else
    {
        state = STATE_UP;
    }
    return false;
}
void Button::mouseDown(const SDL_Event &event)
{
    if(mouseInside(event.button.x, event.button.y))
    {
        state = STATE_DOWN;
    }
}
void Button::mouseMove(const SDL_Event &event)
{
    if(mouseInside(event.motion.x, event.motion.y))
    {
        state = STATE_OVER;
    }
    else
    {
        state = STATE_UP;
    }
}

bool Button::handleClick()
{
    printf("Click\n");
    return false;
}

////////////////////////////////
bool ExitButton::handleClick()
{
    gRunning = false;
    return true;
}
///////////////////////////////
bool NewGameButton::handleClick()
{
    gGame->switchGameMode(new GM_Level());
    return true;
}
/////////////////////////////////
bool OptionsButton::handleClick()
{
    printf("Options clicked\n");
    return false;
}
////////////////////////////////
bool CreditsButton::handleClick()
{
    gGame->switchGameMode(new GM_Credits());
    return true;
}
////////////////////////////////
bool BackToMainMenuButton::handleClick()
{
    gGame->switchGameMode(new GM_MainMenu());
    return true;
}
