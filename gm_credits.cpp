#include "gm_mainmenu.h"
#ifndef __APPLE__
#include <GL/gl.h>
#include <GL/glu.h>
#else
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#endif
#include "button.h"
#include "layer.h"
#include "gm_credits.h"

extern bool gRunning;
extern SDL_Surface * gScreen;

GM_Credits::GM_Credits()
{
    Layer *layer(new Layer("background.png", gScreen->w / 2,gScreen->h / 2));
    this->layers.push_back(layer);

    Layer *layer2(new Layer("credits.png", gScreen->w / 2, gScreen->h * 1.5));
    this->layers.push_back(layer2);

    Button *exitButton(new BackToMainMenuButton("mainmenubutton.png", 130, 335));
    this->buttons.push_back(exitButton);


}
GM_Credits::~GM_Credits()
{

   for(std::vector<Layer*>::iterator itr = this->layers.begin();
        itr != this->layers.end();
        itr++)
        {
            Layer *bg = *itr;
            delete bg;
        }



    for(std::vector<Button*>::iterator it = this->buttons.begin();
        it != this->buttons.end();
        it++)
    {
        // it se ponasa kao da je: Button** it
        Button * btn = *it;
        delete btn;

        // alternativa: delete *it;
    }
}

void
GM_Credits::keyDown(const SDL_Event& event)
{
    switch(event.key.keysym.sym)
    {
        case SDLK_ESCAPE:
        gRunning = false;
        break;

        default:
        GameMode::keyDown(event);
        break;
    }


}

void
GM_Credits::paintSelf()
{
    glViewport(0, 0, gScreen->w, gScreen->h);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, gScreen->w, gScreen->h, 0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    for(std::vector<Layer*>::iterator itr = this->layers.begin();
        itr != this->layers.end();
        itr++)
    {
        (*itr)->paintSelf();

    }

    for(std::vector<Button*>::iterator it = this->buttons.begin();
        it != this->buttons.end();
        it++)
    {
        (*it)->paintSelf();
    }

    SDL_GL_SwapBuffers();
}

void
GM_Credits::mouseDown(const SDL_Event& event)
{
    if(event.button.x < 30 && event.button.y < 30)
    {
        gRunning = false;
    }

    for(std::vector<Button*>::iterator it = this->buttons.begin();
        it != this->buttons.end();
        it++)
    {
        (*it)->mouseDown(event);
    }

}


void
GM_Credits::mouseUp(const SDL_Event& event)
{
    for(std::vector<Button*>::iterator it = this->buttons.begin();
        it != this->buttons.end();
        it++)
    {
        if((*it)->mouseUp(event))
        {
            return;
        }
    }
}

void
GM_Credits::mouseMove(const SDL_Event& event)
{
    for(std::vector<Button*>::iterator it = this->buttons.begin();
        it != this->buttons.end();
        it++)
    {
        (*it)->mouseMove(event);
    }
}

void
GM_Credits::updateSelf(double timeDelta)
{
    this->layers[1]->setY(this->layers[1]->getY() - 100 * timeDelta);
}

