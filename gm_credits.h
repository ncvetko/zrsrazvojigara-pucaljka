#ifndef PUCALJKA_GM_CREDITS_H
#define PUCALJKA_GM_CREDITS_H

#include <SDL/SDL.h>
#include <vector>
#include "gamemode.h"
#include "gm_credits.h"


class Button;
class Layer;

class GM_Credits : public GameMode
{
    public:
    GM_Credits();
    ~GM_Credits();

    void keyDown(const SDL_Event& event);
    void paintSelf(void);
    void mouseUp(const SDL_Event &event);
    void mouseDown(const SDL_Event &event);
    void mouseMove(const SDL_Event &event);
    void updateSelf(double timeDelta);
    
    private:
    std::vector<Button*> buttons;
    std::vector<Layer*> layers;
};

#endif
