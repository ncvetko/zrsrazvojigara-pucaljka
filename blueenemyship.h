#ifndef PUCALJKA_BLUEENEMYSHIP_H
#define PUCALJKA_BLUEENEMYSHIP_H

#include "enemyship.h"

class BlueEnemyShip : public EnemyShip
{
    public:
    BlueEnemyShip(const double x,
                    const double y);
    ~BlueEnemyShip();

    void updateSelf(const double timeDelta);

    private:
    double baseY;
};

#endif
