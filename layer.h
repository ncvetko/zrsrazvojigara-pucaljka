#ifndef PUCALJKA_LAYER_H
#define PUCALJKA_LAYER_H

#include <string>
#include "texture.h"

class Layer
{
    public:
    Layer(const std::string& textureFile,
           const int x,
           const int y);
    virtual ~Layer();
    virtual void paintSelf();
    virtual void updateSelf(double timeDelta);

    double getX(void) const { return this->x; }
    double getY(void) const { return this->y; }

    void setX(double x) { this->x = x; }
    void setY(double y) { this->y = y; }

    protected:
    Texture * texture;
    double x, y;
};


#endif
