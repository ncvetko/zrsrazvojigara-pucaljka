OBJS=asteroid.o \
audiomgr.o \
blueenemyship.o \
bullet.o \
button.o \
enemyship.o \
explosion.o \
gamemode.o \
gameobject.o \
gm_credits.o \
gm_level.o \
gm_mainmenu.o \
layer.o \
levelbackground.o \
playership.o \
pucaljka.o \
redenemyship.o \
texture.o \
texture_manager.o \
yellowenemyship.o \

CXXFLAGS=-DHAVE_SDL_MIXER=1

pucaljka: $(OBJS)
	g++ $(OBJS) -lSDL -lSDL_image -lSDL_mixer -lGL -lGLU -o pucaljka
clean:
	rm -f pucaljka $(OBJS)

