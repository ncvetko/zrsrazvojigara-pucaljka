#ifndef PUCALJKA_BULLET_H
#define PUCALJKA_BULLET_H
#include <SDL/SDL_events.h>
#include <string>
#include "gameobject.h"


class Texture;

class Bullet : public GameObject
{
    public:
    Bullet( const double x,
            const double y);
    virtual ~Bullet();

    virtual void paintSelf(void);


    virtual void updateSelf(const double timeDelta);

    private:
    Texture*texture;
    double x,y;
};

#endif


