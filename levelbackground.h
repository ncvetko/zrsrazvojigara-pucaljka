#ifndef PUCALJKA_LEVELBACKGROUND_H
#define PUCALJKA_LEVELBACKGROUND_H

#include <string>
#include "texture.h"
#include "layer.h"


class LevelBackground : public Layer
{
    public:
    LevelBackground(const int x,
                    const int y);
    virtual ~LevelBackground();
    void paintSelf();
    void updateSelf(const double timeDelta);

    private:
    double xOffset;
    double yOffset;
};


#endif
