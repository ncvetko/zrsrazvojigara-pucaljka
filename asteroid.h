#ifndef PUCALJKA_ASTEROID_H
#define PUCALJKA_ASTEROID_H
#include <string>
class Texture;

class Asteroid
{
    public:
    Asteroid(const double x,
             const double y,
             const double vy);
    ~Asteroid();

    void paintSelf();
    void updateSelf(const double timeDelta);

    private:
    double x, y;
    double vy;
    Texture * texture;
};


#endif

