#ifndef PUCALJKA_YELLOWENEMYSHIP_H
#define PUCALJKA_YELLOWENEMYSHIP_H

#include "enemyship.h"

class YellowEnemyShip : public EnemyShip
{
    public:
    YellowEnemyShip(const double x,
                    const double y);
    ~YellowEnemyShip();
    
    void updateSelf(const double timeDelta);
    
    private:
    double defaultY;
    
};

#endif