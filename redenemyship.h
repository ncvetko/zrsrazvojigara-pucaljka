#ifndef PUCALJKA_REDENEMYSHIP_H
#define PUCALJKA_REDENEMYSHIP_H

#include "enemyship.h"

class RedEnemyShip : public EnemyShip
{
    public:
    RedEnemyShip(const double x,
                    const double y);
    ~RedEnemyShip();

    void updateSelf(const double timeDelta);

    private:
    double baseY;
};

#endif

