Pucaljka
========

Pucaljka je projekt razvijen na radionici razvoja
igara u Zagrebačkom računalnom savezu u školskoj
godini 2012/2013.

Sudjeluju:

* Ivan Vučica - voditelj
* Filip Mravunac
* Dino Drokan
* Hrvoje Abramović
* Nikola Cvetko
* Mateo Imbrišak
* Felix Bečeić
* Mislav Graonić

Vrijedi MIT licenca.

Glazba
======

Mad Mav - Torture: licencirano pod CC BY-ND 3.0. Preuzeto s http://www.jamendo.com/en/track/181453/torture

Zvuk
====

gun.wav: "m16 gun fire, 3 round burst and single fire", This work is licensed under the Creative Commons 0 License. Preuzeto s http://freesound.org/people/lensflare8642/sounds/145206/ i izrezano. Autor: lensflare8642

