#include "explosion.h"
#include "texture_manager.h"

Explosion::Explosion(const std::string& textureFile,
                     const double argX,
                     const double argY) : texture(TextureManager::getInstance()->getTexture(textureFile)),
                                            x(argX), y(argY), t(0)
{
    this->texture->retain();
}
Explosion::~Explosion()
{
    this->texture->release();
}
void
Explosion::calcTexCoords(int spriteOffX, int spriteOffY,
                         float wImg, float hImg,
                         int wSprite, int hSprite,
                         int animation, int frame,
                         float *txTop, float *txBottom, float *txLeft, float *txRight)
{
    int animX = animation % 3, animY = animation / 3;
    int pxAnimX = animX * wSprite * 4, pxAnimY = animY * hSprite * 4;

    int frameX = frame % 4, frameY = frame / 4;
    int pxFrameX = frameX * wSprite, pxFrameY = frameY * hSprite;

    int pxLeft = spriteOffX + pxAnimX + pxFrameX;
    int pxRight = pxLeft + wSprite;
    int pxTop = spriteOffY + pxAnimY + pxFrameY;
    int pxBottom = pxTop + hSprite;

    //printf("PX L: %d R: %d T: %d B: %d\n", pxLeft, pxRight, pxTop, pxBottom);

    float left = pxLeft / (float)wImg;
    float right = pxRight / (float)wImg;
    float top = pxTop / (float)hImg;
    float bottom = pxBottom / (float)hImg;

    //printf("TX L: %g R: %g T: %g B: %g\n", left, right, top, bottom);

    *txLeft = left;
    *txRight = right;
    *txTop = top;
    *txBottom = bottom;
}

void
Explosion::paintSelf()
{
    if(t >= 1)
        return;

    ///////////

    float txTop = 0, txBottom = 0, txLeft = 0, txRight = 0;

    calcTexCoords(0, 0, 256, 256, 64, 64, 0, t * 16, &txTop, &txBottom, &txLeft, &txRight);

    this->texture->bind();
    glEnable(GL_TEXTURE_2D);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(this->x, this->y, 0);

    glScalef(50, 50, 1);
    glBegin(GL_TRIANGLES);


    glTexCoord2f(txLeft, txBottom);
    glVertex2f(-0.5, 0.5);
    glTexCoord2f(txLeft, txTop);
    glVertex2f(-0.5, -0.5);
    glTexCoord2f(txRight, txTop);
    glVertex2f(0.5, -0.5);

    glTexCoord2f(txRight, txTop);
    glVertex2f(0.5, -0.5);
    glTexCoord2f(txRight, txBottom);
    glVertex2f(0.5, 0.5);
    glTexCoord2f(txLeft, txBottom);
    glVertex2f(-0.5, 0.5);

    glEnd();
    glPopMatrix();

}
void
Explosion::updateSelf(const double timeDelta)
{
    GameObject::updateSelf(timeDelta);
    t += timeDelta/4.86 ;
    if(t >= 1)
        die();
}


