#ifndef PUCALJKA_ENEMYSHIP_H
#define PUCALJKA_ENEMYSHIP_H

#include <SDL/SDL_events.h>
#include <string>
#include "texture.h"
#include "gameobject.h"
#include "types.h"

class EnemyShip : public GameObject
{
    public:
    EnemyShip(const std::string& textureFile,
               const double x,
               const double y);

    virtual ~EnemyShip();

    virtual void paintSelf(void);
    virtual void updateSelf(const double timeDelta);

    double getShipW() const { return 64; }
    double getShipH() const { return 64; }

    PUCRect getRect() const
    {
        PUCRect r;
        r.x = x - this->getShipW() / 2;
        r.y = y - this->getShipH() / 2;
        r.w = this->getShipW();
        r.h = this->getShipH();

        return r;
    }
    double x,y;
    private:
    Texture*texture;
    protected:

    float angle;
};

extern bool gDrawCollisionRects;

#endif

