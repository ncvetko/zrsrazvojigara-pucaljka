#ifndef PUCALJKA_GAMEOBJECT_H
#define PUCALJKA_GAMEOBJECT_H

#include <SDL/SDL.h>

class GM_Level;

class GameObject
{
    public:
    GameObject() : shouldDie(false)
    {
    }
    virtual ~GameObject()
    {
    }

    virtual void keyDown(const SDL_Event& event)
    {
    }
    virtual void keyUp(const SDL_Event& event)
    {
    }
    virtual void mouseDown(const SDL_Event &event)
    {
    }
    virtual void mouseUp(const SDL_Event &event)
    {
    }
    virtual void mouseMove(const SDL_Event &event)
    {
    }

    virtual void paintSelf(void)
    {
    }
    virtual void updateSelf(double timeDelta)
    {
    }

    virtual void die(void)
    {
        this->shouldDie = true;
    }
    bool isDead(void) const
    {
        return this->shouldDie;
    }

    GM_Level * getOwnerLevel();

    private:
    bool shouldDie;
};

#endif

